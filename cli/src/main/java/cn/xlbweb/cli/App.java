package cn.xlbweb.cli;

import cn.xlbweb.cli.common.CliProperties;
import cn.xlbweb.util.AddrUtils;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@SpringBootApplication
@MapperScan("cn.xlbweb.cli.mapper")
@Slf4j
public class App implements ApplicationRunner {

    @Autowired
    private CliProperties cliProperties;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Tomcat start success! addr: http://{}:{}", AddrUtils.getLocalIp(), cliProperties.getPort());
        log.info("Swagger3 api addr: http://{}:{}{}", AddrUtils.getLocalIp(), cliProperties.getPort(), cliProperties.getSwaggerUri());
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
