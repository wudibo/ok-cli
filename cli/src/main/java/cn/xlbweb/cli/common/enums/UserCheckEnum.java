package cn.xlbweb.cli.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:13
 * @description:
 */
@AllArgsConstructor
@Getter
public enum UserCheckEnum {

    UN_LOGIN(2000, "用户未登录"),
    UNKNOWN_ACCOUNT(2001, "账号不存在"),
    INCORRECT_CREDENTIALS(2002, "密码不正确"),
    DISABLED_ACCOUNT(2003, "账号被禁用"),
    LOGIN_FAILED_TOO_MUCH(2004, "账号登录次数过多，请三分钟后再试"),
    UN_AUTH(2005, "用户无权限");

    private int key;
    private String value;
}
