package cn.xlbweb.cli.common;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author: bobi
 * @date: 2021/5/9 下午8:38
 * @description:
 */
@Data
@Configuration
public class CliProperties {

    @Value("${server.port}")
    private String port;

    @Value("${cn.xlbweb.cli.swagger-uri}")
    private String swaggerUri;

    @Value("${cn.xlbweb.cli.algorithm-name}")
    private String algorithmName;

    @Value("${cn.xlbweb.cli.hash-iterations}")
    private Integer hashIterations;

    @Value("${cn.xlbweb.cli.exclude-uri}")
    private String excludeUri;

    @Value("${cn.xlbweb.cli.token-name}")
    private String tokenName;
}
