package cn.xlbweb.cli.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:13
 * @description:
 */
@AllArgsConstructor
@Getter
public enum UserStatusEnum {

    NORMAL(0, "账号正常"),
    DISABLE(1, "账号被禁用"),
    DELETE(2, "账号被删除");

    private int key;
    private String value;
}
