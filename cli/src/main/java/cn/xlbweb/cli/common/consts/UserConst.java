package cn.xlbweb.cli.common.consts;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:12
 * @description:
 */
public class UserConst {

    /**
     * 超级管理员
     */
    public static final String ADMIN_USERNAME = "admin";
}
