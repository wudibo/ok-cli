package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.pojo.model.UserRole;

import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午11:20
 * @description:
 */
public interface UserRoleMapper {

    int insertUserRole(List<UserRole> userRoles);

    void deleteUserRole(Integer userId);
}
