package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.pojo.model.RolePermission;

import java.util.Arrays;
import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午11:20
 * @description:
 */
public interface RolePermissionMapper {

    int insertRolePermission(List<RolePermission> rolePermissions);

    default int deleteRolePermission(Integer roleId) {
        return batchDeleteRolePermission(Arrays.asList(roleId));
    }

    int batchDeleteRolePermission(List<Integer> roleIdArr);
}
