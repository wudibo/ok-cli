package cn.xlbweb.cli.mapper;


import cn.xlbweb.cli.pojo.dto.PermissionListDTO;
import cn.xlbweb.cli.pojo.model.Permission;
import cn.xlbweb.cli.pojo.vo.PermissionVO;

import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午11:20
 * @description:
 */
public interface PermissionMapper {

    List<String> listPermissionUri(Integer userId);

    PermissionVO getPermission(Integer id);

    List<PermissionVO> listPermission(PermissionListDTO dto);

    int insertPermission(Permission permission);

    int updatePermission(Permission permission);

    int batchDeletePermisson(List<Integer> idList);
}
