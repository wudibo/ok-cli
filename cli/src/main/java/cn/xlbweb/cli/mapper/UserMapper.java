package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.model.User;
import cn.xlbweb.cli.pojo.vo.UserVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午11:20
 * @description:
 */
public interface UserMapper {

    User getUserByUsername(String username);

//    @SuppressWarnings(value = "all")
//    default UserVO getUserById(Integer id) {
//        return getUser(User.builder().id(id).build());
//    }
//
//    UserVO getUser(User user);

    /**
     * 当返回的vo比较复杂时，为避免写重写sql，故共用listUser()方法
     *
     * @param id
     * @return
     */
    @SuppressWarnings(value = "all")
    default UserVO getUserById(Integer id) {
        List<UserVO> vos = listUser(UserListDTO.builder().id(id).build());
        return !CollectionUtils.isEmpty(vos) ? vos.get(0) : null;
    }

    List<UserVO> listUser(UserListDTO dto);

    int insertUser(User user);

    int updateUser(User user);

    int deleteUser(@Param("idArr") String[] idArr, @Param("status") int status);
}
