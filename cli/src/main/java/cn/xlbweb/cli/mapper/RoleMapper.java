package cn.xlbweb.cli.mapper;


import cn.xlbweb.cli.pojo.dto.RoleListDTO;
import cn.xlbweb.cli.pojo.model.Role;
import cn.xlbweb.cli.pojo.vo.RoleVO;

import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午11:20
 * @description:
 */
public interface RoleMapper {

    RoleVO getRole(Integer id);

    List<RoleVO> listRole(RoleListDTO dto);

    int insertRole(Role role);

    void updateRole(Role role);

    int deleteRole(List<Integer> idList);
}
