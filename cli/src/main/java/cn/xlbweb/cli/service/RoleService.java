package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.*;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:08
 * @description:
 */
public interface RoleService {

    ServerResponse getRole(Integer id);

    TableResponse listRole(RoleListDTO dto);

    ServerResponse insertRole(RoleEditDTO dto);

    ServerResponse updateRole(RoleEditDTO dto);

    ServerResponse deleteRole(String ids);
}
