package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.UserEditDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:08
 * @description:
 */
public interface UserService {

    ServerResponse getUser(Integer id);

    TableResponse listUser(UserListDTO dto);

    ServerResponse insertUser(UserEditDTO dto);

    ServerResponse updateUser(UserEditDTO dto);

    ServerResponse deleteUser(String ids);
}
