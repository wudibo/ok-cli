package cn.xlbweb.cli.service;

import cn.xlbweb.cli.pojo.dto.PermissionEditDTO;
import cn.xlbweb.cli.pojo.dto.PermissionListDTO;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;

import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:10
 * @description:
 */
public interface PermissionService {

    default List<String> listPermissionUri() {
        return listPermissionUri(null);
    }

    List<String> listPermissionUri(Integer userId);

    ServerResponse getPermission(Integer id);

    TableResponse listPermission(PermissionListDTO dto);

    ServerResponse insertPermission(PermissionEditDTO dto);

    ServerResponse updatePermission(PermissionEditDTO dto);

    ServerResponse deletePermission(String ids);
}
