package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.common.enums.UserStatusEnum;
import cn.xlbweb.cli.mapper.RoleMapper;
import cn.xlbweb.cli.mapper.UserMapper;
import cn.xlbweb.cli.mapper.UserRoleMapper;
import cn.xlbweb.cli.pojo.dto.UserEditDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.model.User;
import cn.xlbweb.cli.pojo.model.UserRole;
import cn.xlbweb.cli.pojo.vo.RoleVO;
import cn.xlbweb.cli.pojo.vo.UserVO;
import cn.xlbweb.cli.service.UserService;
import cn.xlbweb.util.BeanUtils;
import cn.xlbweb.util.IdUtils;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:08
 * @description:
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public ServerResponse getUser(Integer id) {
        UserVO vo = userMapper.getUserById(id);
        if (Objects.isNull(vo)) {
            return ServerResponse.error("用户信息不存在");
        }
        return ServerResponse.success("用户信息查询成功", vo);
    }

    @Override
    public TableResponse listUser(UserListDTO dto) {
        PageHelper.startPage(dto.getPage(), dto.getSize());
        List<UserVO> vos = userMapper.listUser(dto);
        return TableResponse.success(vos.size(), vos);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse insertUser(UserEditDTO dto) {
        // 校验账号是否重复
        User checkUser = userMapper.getUserByUsername(dto.getUsername());
        if (Objects.nonNull(checkUser)) {
            return ServerResponse.error("账号已存在");
        }

        // 插入用户
        User user = BeanUtils.copyProperties(dto, User::new);
        userMapper.insertUser(user);

        // 插入用户角色
        List<UserRole> userRoles = assembleUserRole(user.getId(), dto.getRoleIds());
        int count = userRoleMapper.insertUserRole(userRoles);
        if (count > 0) {
            return ServerResponse.success("新增用户成功");
        }
        return ServerResponse.error("新增用户失败");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse updateUser(UserEditDTO dto) {
        // 更新用户
        User user = BeanUtils.copyProperties(dto, User::new);
        userMapper.updateUser(user);

        // 删除原有角色
        userRoleMapper.deleteUserRole(dto.getId());

        // 插入新增角色
        List<UserRole> userRoles = assembleUserRole(user.getId(), dto.getRoleIds());
        int count = userRoleMapper.insertUserRole(userRoles);
        if (count > 0) {
            return ServerResponse.success("更新用户成功");
        }
        return ServerResponse.error("更新用户失败");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse deleteUser(String ids) {
        String[] idArr = StringUtils.split(ids, ",");
        // 逻辑删除
        int count = userMapper.deleteUser(idArr, UserStatusEnum.DELETE.getKey());
        if (count > 0) {
            return ServerResponse.success("删除用户成功");
        }
        return ServerResponse.error("删除用户失败");
    }

    private List<UserRole> assembleUserRole(Integer userId, String roleIds) {
        List<Integer> roleIdArr = IdUtils.toListInteger(roleIds);
        List<UserRole> result = new ArrayList<>();
        UserVO userVO = userMapper.getUserById(userId);
        for (Integer roleId : roleIdArr) {
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);
            // 备注
            StringBuilder remarks = new StringBuilder();
            RoleVO roleVO = roleMapper.getRole(roleId);
            remarks.append(userVO.getUsername()).append("-").append(roleVO.getName());
            userRole.setRemarks(remarks.toString());
            result.add(userRole);
        }
        return result;
    }
}
