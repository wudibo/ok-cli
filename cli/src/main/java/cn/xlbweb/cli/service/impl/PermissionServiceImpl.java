package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.mapper.PermissionMapper;
import cn.xlbweb.cli.pojo.dto.PermissionEditDTO;
import cn.xlbweb.cli.pojo.dto.PermissionListDTO;
import cn.xlbweb.cli.pojo.model.Permission;
import cn.xlbweb.cli.pojo.vo.PermissionVO;
import cn.xlbweb.cli.service.PermissionService;
import cn.xlbweb.util.BeanUtils;
import cn.xlbweb.util.IdUtils;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:10
 * @description:
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<String> listPermissionUri(Integer userId) {
        return permissionMapper.listPermissionUri(userId);
    }

    @Override
    public ServerResponse getPermission(Integer id) {
        PermissionVO vo = permissionMapper.getPermission(id);
        return ServerResponse.success("权限信息查询成功", vo);
    }

    @Override
    public TableResponse listPermission(PermissionListDTO dto) {
        PageHelper.startPage(dto.getPage(), dto.getSize());
        List<PermissionVO> vos = permissionMapper.listPermission(dto);
        return TableResponse.success(vos.size(), vos);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse insertPermission(PermissionEditDTO dto) {
        Permission permission = BeanUtils.copyProperties(dto, Permission::new);
        int count = permissionMapper.insertPermission(permission);
        if (count > 0) {
            return ServerResponse.success("新增权限成功");
        }
        return ServerResponse.error("新增权限失败");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse updatePermission(PermissionEditDTO dto) {
        Permission permission = BeanUtils.copyProperties(dto, Permission::new);
        int count = permissionMapper.updatePermission(permission);
        if (count > 0) {
            return ServerResponse.success("更新权限成功");
        }
        return ServerResponse.error("更新权限失败");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse deletePermission(String ids) {
        int count = permissionMapper.batchDeletePermisson(IdUtils.toListInteger(ids));
        if (count > 0) {
            return ServerResponse.success("删除权限成功");
        }
        return ServerResponse.error("删除权限失败");
    }
}
