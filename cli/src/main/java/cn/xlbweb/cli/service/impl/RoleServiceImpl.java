package cn.xlbweb.cli.service.impl;

import cn.xlbweb.cli.mapper.PermissionMapper;
import cn.xlbweb.cli.mapper.RoleMapper;
import cn.xlbweb.cli.mapper.RolePermissionMapper;
import cn.xlbweb.cli.pojo.dto.*;
import cn.xlbweb.cli.pojo.model.Role;
import cn.xlbweb.cli.pojo.model.RolePermission;
import cn.xlbweb.cli.pojo.vo.RoleVO;
import cn.xlbweb.cli.service.RoleService;
import cn.xlbweb.util.BeanUtils;
import cn.xlbweb.util.IdUtils;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:08
 * @description:
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public ServerResponse getRole(Integer id) {
        RoleVO vo = roleMapper.getRole(id);
        return ServerResponse.success("角色信息查询成功", vo);
    }

    @Override
    public TableResponse listRole(RoleListDTO dto) {
        PageHelper.startPage(dto.getPage(), dto.getSize());
        List<RoleVO> vos = roleMapper.listRole(dto);
        return TableResponse.success(vos.size(), vos);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse insertRole(RoleEditDTO dto) {
        // 插入角色
        Role role = BeanUtils.copyProperties(dto, Role::new);
        roleMapper.insertRole(role);

        // 插入角色权限
        List<RolePermission> rolePermissions = assembleRolePermission(role.getId(), dto.getPermissionIds());
        int count = rolePermissionMapper.insertRolePermission(rolePermissions);
        if (count > 0) {
            return ServerResponse.success("新增角色成功");
        }
        return ServerResponse.error("新增角色失败");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse updateRole(RoleEditDTO dto) {
        // 更新角色
        Role role = BeanUtils.copyProperties(dto, Role::new);
        roleMapper.updateRole(role);

        // 删除原有权限
        rolePermissionMapper.deleteRolePermission(dto.getId());

        // 插入新增权限
        List<RolePermission> rolePermissions = assembleRolePermission(role.getId(), dto.getPermissionIds());
        int count = rolePermissionMapper.insertRolePermission(rolePermissions);
        if (count > 0) {
            return ServerResponse.success("更新角色成功");
        }
        return ServerResponse.error("更新角色失败");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServerResponse deleteRole(String ids) {
        List<Integer> idList = IdUtils.toListInteger(ids);
        // 删除角色
        int count = roleMapper.deleteRole(idList);

        // 删除权限
        rolePermissionMapper.batchDeleteRolePermission(idList);
        if (count > 0) {
            return ServerResponse.success("删除角色成功");
        }
        return ServerResponse.error("删除角色失败");
    }

    private List<RolePermission> assembleRolePermission(Integer roleId, String permissionIds) {
        List<Integer> permissionIdArr = IdUtils.toListInteger(permissionIds);
        List<RolePermission> result = new ArrayList<>();
        RoleVO roleVO = roleMapper.getRole(roleId);
        for (Integer permissionId : permissionIdArr) {
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermission.setPermissionId(permissionId);
            // 备注
            StringBuilder remarks = new StringBuilder();
            remarks.append(roleVO.getName()).append("-").append("rolename...");
            rolePermission.setRemarks(remarks.toString());
            result.add(rolePermission);
        }
        return result;
    }
}
