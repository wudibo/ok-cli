package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.*;
import cn.xlbweb.cli.service.PermissionService;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: bobi
 * @date: 2019-02-04 00:55
 * @description:
 */
@RestController
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @GetMapping("/permission/{id}")
    public ServerResponse getPermission(@PathVariable("id") Integer id) {
        return permissionService.getPermission(id);
    }

    @GetMapping("/permissions")
    public TableResponse listPermission(@Validated PermissionListDTO dto) {
        return permissionService.listPermission(dto);
    }

    @PostMapping("/permission")
    public ServerResponse insertPermission(@Validated PermissionEditDTO dto) {
        return permissionService.insertPermission(dto);
    }

    @PutMapping("/permission")
    public ServerResponse updatePermission(@Validated PermissionEditDTO dto) {
        return permissionService.updatePermission(dto);
    }

    @DeleteMapping("/permission/{ids}")
    public ServerResponse deletePermission(@PathVariable("ids") String ids) {
        return permissionService.deletePermission(ids);
    }
}

