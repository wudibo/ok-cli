package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.UserLoginDTO;
import cn.xlbweb.util.response.ServerResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: bobi
 * @date: 2019-02-04 00:55
 * @description:
 */
@RestController
public class LoginController {

    @PostMapping("/login")
    public ServerResponse login(@Validated UserLoginDTO dto) {
        UsernamePasswordToken token = new UsernamePasswordToken(dto.getUsername(), dto.getPassword());
        Subject subject = SecurityUtils.getSubject();
        subject.login(token);
        return ServerResponse.success("登陆成功", subject.getSession().getId());
    }

    @DeleteMapping("/logout")
    public ServerResponse logout() {
        SecurityUtils.getSubject().logout();
        return ServerResponse.success("退出成功");
    }
}

