package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.*;
import cn.xlbweb.cli.pojo.dto.base.Insert;
import cn.xlbweb.cli.pojo.dto.base.Update;
import cn.xlbweb.cli.service.RoleService;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: bobi
 * @date: 2019-02-04 00:55
 * @description:
 */
@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/role/{id}")
    public ServerResponse getRole(@PathVariable("id") Integer id) {
        return roleService.getRole(id);
    }

    @GetMapping("/roles")
    public TableResponse listRole(@Validated RoleListDTO dto) {
        return roleService.listRole(dto);
    }

    @PostMapping("/role")
    public ServerResponse insertRole(@Validated(Insert.class) RoleEditDTO dto) {
        return roleService.insertRole(dto);
    }

    @PutMapping("/role")
    public ServerResponse updateRole(@Validated(Update.class) RoleEditDTO dto) {
        return roleService.updateRole(dto);
    }

    @DeleteMapping("/role/{ids}")
    public ServerResponse deleteRole(@PathVariable("ids") String ids) {
        return roleService.deleteRole(ids);
    }
}

