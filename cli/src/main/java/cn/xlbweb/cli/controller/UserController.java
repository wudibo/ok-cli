package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.pojo.dto.UserEditDTO;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.dto.base.Insert;
import cn.xlbweb.cli.pojo.dto.base.Update;
import cn.xlbweb.cli.service.UserService;
import cn.xlbweb.util.response.ServerResponse;
import cn.xlbweb.util.response.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: bobi
 * @date: 2019-02-04 00:55
 * @description:
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public ServerResponse getUser(@PathVariable("id") Integer id) {
        return userService.getUser(id);
    }

    @GetMapping("/users")
    public TableResponse listUser(@Validated UserListDTO dto) {
        return userService.listUser(dto);
    }

    @PostMapping("/user")
    public ServerResponse insertUser(@Validated(Insert.class) UserEditDTO dto) {
        return userService.insertUser(dto);
    }

    @PutMapping("/user")
    public ServerResponse updateUser(@Validated(Update.class) UserEditDTO dto) {
        return userService.updateUser(dto);
    }

    @DeleteMapping("/user/{ids}")
    public ServerResponse deleteUser(@PathVariable("ids") String ids) {
        return userService.deleteUser(ids);
    }
}

