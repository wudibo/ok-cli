package cn.xlbweb.cli.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:19
 * @description:
 */
@Data
public class UserLoginDTO {

    @NotBlank(message = "账号不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;
}
