package cn.xlbweb.cli.pojo.dto;

import cn.xlbweb.cli.pojo.dto.base.Update;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:19
 * @description:
 */
@Data
public class RoleEditDTO {

    @NotNull(message = "主键ID不能为空", groups = {Update.class})
    private Integer id;

    @NotNull(message = "角色名称不能为空")
    private String name;

    private String permissionIds;
}
