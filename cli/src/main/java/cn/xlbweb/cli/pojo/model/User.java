package cn.xlbweb.cli.pojo.model;

import lombok.*;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:10
 * @description:
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class User {

    private Integer id;

    private String username;

    private String password;

    private String email;

    private Integer status;

    private Date createTime;

    private Date updateTime;
}
