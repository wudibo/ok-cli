package cn.xlbweb.cli.pojo.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:19
 * @description:
 */
@Data
public class UserVO {

    private Integer id;

    private String username;

    private String email;

    private Integer status;

    private Date createTime;

    private Date updateTime;
}
