package cn.xlbweb.cli.pojo.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:19
 * @description:
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class UserListDTO {

    @NotNull(message = "当前页不能为空")
    private Integer page;

    @NotNull(message = "每页显示数量不能为空")
    private Integer size;

    private Integer id;

    private String username;

    private String email;

    private Integer status;
}
