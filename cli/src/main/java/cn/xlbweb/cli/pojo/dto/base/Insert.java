package cn.xlbweb.cli.pojo.dto.base;

import javax.validation.groups.Default;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:19
 * @description:
 */
public interface Insert extends Default {
}
