package cn.xlbweb.cli.pojo.vo;

import lombok.Data;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:19
 * @description:
 */
@Data
public class RoleVO {

    private String name;
}
