package cn.xlbweb.cli.pojo.dto;

import cn.xlbweb.cli.pojo.dto.base.Insert;
import cn.xlbweb.cli.pojo.dto.base.Update;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:19
 * @description:
 */
@Data
public class UserEditDTO {

    @NotNull(message = "主键ID不能为空", groups = {Update.class})
    private Integer id;

    @NotBlank(message = "账号不能为空", groups = {Insert.class})
    private String username;

    @NotBlank(message = "密码不能为空", groups = {Insert.class})
    private String password;

    // @NotBlank(message = "用户角色不能为空", groups = {Insert.class, Update.class})
    // 未分组则等同于全部校验
    @NotBlank(message = "用户角色不能为空")
    private String roleIds;
}
