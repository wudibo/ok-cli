package cn.xlbweb.cli.pojo.model;

import lombok.Data;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2021/5/9 下午9:10
 * @description:
 */
@Data
public class UserRole {

    private Integer id;

    private Integer userId;

    private Integer roleId;

    private String remarks;

    private Date createTime;

    private Date updateTime;
}
