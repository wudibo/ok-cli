package cn.xlbweb.cli.mapper;

import cn.xlbweb.cli.AppTests;
import cn.xlbweb.cli.pojo.dto.UserListDTO;
import cn.xlbweb.cli.pojo.model.User;
import cn.xlbweb.cli.pojo.vo.UserVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author: bobi
 * @date: 2021/5/9 下午11:23
 * @description:
 */
public class UserMapperTest extends AppTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void listUser() {
        UserListDTO dto = UserListDTO.builder().page(1).size(20).build();
        List<UserVO> vos = userMapper.listUser(dto);
         System.out.println(vos);
    }

    @Test
    public void insertUser() {
        User user = User.builder().username("admin").password("123456").email("admin@qq.com").status(0).build();
        int count = userMapper.insertUser(user);
        System.out.println(count);
    }
}