<p align="center">
    <img src="https://images.gitee.com/uploads/images/2020/0501/183741_faaba28a_1152471.png"/>
    <p align="center">
        一个基于RBAC设计的前后端分离开发脚手架~
        <br/>
        内嵌数据库，开箱即用，可基于此脚手架进行二次开发，无需每次再考虑基础模块的重复开发。 :facepunch: 
    </p>
    <p align="center">
        <img src="https://img.shields.io/badge/jdk-1.8-brightgreen">
        <img src="https://img.shields.io/badge/sqlite-3-brightgreen">
        <img src="https://img.shields.io/badge/maven-3.6.1-brightgreen">
        <img src="https://img.shields.io/badge/node-12.16.1-brightgreen">
        <img src="https://img.shields.io/badge/npm-6.13.4-brightgreen">
        <img src="https://img.shields.io/badge/vue--cli-4.2.3-brightgreen">
    </p>
    <p align="center">
        <img src="https://img.shields.io/badge/SpringBoot-2.2.5.RELEASE-brightgreen">
        <img src="https://img.shields.io/badge/MyBatis-3.5.2-brightgreen">
        <img src="https://img.shields.io/badge/Shiro-1.4.1-brightgreen">
        <img src="https://img.shields.io/badge/Element%20UI-2.13.0-brightgreen">
        <img src="https://img.shields.io/badge/LICENSE-MIT-yellowgreen">
    </p>
</p>

---

#### QQ交流群

964252127 🔥

#### 项目特点

- 基于SpringBoot 2.4.5版本开发
- 基于Shiro实现用户认证、授权
- 基于Shiro实现授权缓存
- 基于Shiro实现密码加密（MD5加密+hash次数+基于账号的salt加密）
- 基于Shiro实现登录次数限制
- 重写FormAuthenticationFilter和DefaultWebSessionManager，前后端分离开发，使用token形式交互
- 集成Swagger3 API在线文档
- 操作日志记录
- 用户、角色、权限模块功能开发
- 多环境打包配置
- 多环境日志组件配置
- 使用内嵌数据库，不用额外引入MySQL；使用内存缓存Ehcache，不用额外引入Redis

#### 后端组件

| 类别 | 使用版本 | 最新版本 | 官网地址 |
|-------|-----|------|------|
| Java SE | 8 | 15 | https://www.oracle.com/java/technologies/javase-downloads.html |
| SQLite | 3.35.5 | 3.35.5 | https://www.sqlite.org/download.html |
| Maven | 3.6.1 | 3.6.3 | http://maven.apache.org/download.cgi |
| IDEA | 2021.1.1 | 2021.1.1 | https://www.jetbrains.com/idea/download |


#### 前端组件

| 类别 | 使用版本 | 最新版本 | 官网地址 |
|-------|-----|------|------|
| Node JS | 12.16.1 | 14.15.1 | https://nodejs.org/en/download |
| NPM | 6.13.4 | 6.14.8 | https://nodejs.org/en/download |
| VUE-CLI | 4.23.3 | 4.5.9 | https://github.com/vuejs/vue-cli/releases |
| WebStorm | 2021.1.1 | 2021.1.1 | https://www.jetbrains.com/webstorm/download |

#### 界面预览

待补充...

#### 扩展资料

https://gitee.com/wudibo/ok-cli/wikis/pages

#### 开源协议

[MIT](https://gitee.com/wudibo/ok-cli/blob/v1.0/LICENSE)